#!/bin/sh

# Remove unnecessary files and folders
[ -d "/CONTROL" ] && rm -r /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1

echo ""
echo "> Checking mounted storage, please wait..."
sleep 3

# Check for mounted storage
msp=("/media/hdd" "/media/usb" "/usr/share/enigma2")
for ms in "${msp[@]}"; do
    if [ -d "$ms" ]; then
        echo "Mounted storage found at: $ms"
        break
    fi
done

# Create picon directory if not exists
if [ ! -d "$ms/picon" ]; then
    mkdir -p $ms/picon
fi

# Download and install picons
echo ""
echo "> Downloading & installing picons please wait..."
sleep 5
plugin="picons"
version="16e"
url="https://gitlab.com/hanfy1971/picons/-/raw/main/$plugin-$version.tar.gz"
package="$ms/picon/$plugin-$version.tar.gz"
wget --show-progress -qO $package --no-check-certificate $url
tar -xzf $package -C $ms/picon >/dev/null 2>&1
extract=$?
rm -rf $package >/dev/null 2>&1

echo ""
if [ $extract -eq 0 ]; then
    echo "> $plugin-$version package installed successfully"
    sleep 3
else
    echo "> $plugin-$version package installation failed"
    sleep 3
fi
